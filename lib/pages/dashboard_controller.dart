import 'package:get/get.dart';
import 'package:nextbase_assignent/model/piece.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:retry/retry.dart';
import 'dart:async';
import 'dart:io';
import 'package:latlong2/latlong.dart';

class DashboardController extends GetxController {
  int pageNo = 0;
  var tabIndex = 0;
  RxList<Piece> data = RxList<Piece>();

  List<LatLng> getPoints() => data.map((p) => LatLng(p.lat, p.lon)).toList();
  LatLng getCenterPoint() {
    if (data.isEmpty) {
      return LatLng(51.57281, -3.20787);
    } else {
      return LatLng(data.last.lat, data.last.lon);
    }
  }

  void changeTabIndex(int index) {
    tabIndex = index;
    update();
  }

  getData(int n) async {
    print('getData');
    if (n < 1) data = RxList<Piece>();
    pageNo = n;

    var uri = Uri.parse(
        'https://us-central1-mynextbase-connect.cloudfunctions.net/sampleData?page=' +
            pageNo.toString());
    const r = RetryOptions(maxAttempts: 40);
    final response = await r.retry(
        () => http.get(uri).timeout(const Duration(seconds: 5)),
        retryIf: (e) => e is SocketException || e is TimeoutException);

    try {
      var j = json.decode(response.body);
      List<Piece> pcs = parseData(response.body);

      var d = data.toList();
      List<Piece> d1 = d + pcs;
      if (pageNo != n) return;
      data = RxList.from(d1);

      print(pageNo);
      update();
      if (j['hasMore']) {
        pageNo++;
        getData(pageNo);
      }
    } catch (e) {
      print(e);
      getData(pageNo);
    }
  }

  parseData(String r) {
    var j = json.decode(r);
    Iterable data = j['data'];
    return
      List<Piece>.from(data.map((model) => Piece.fromJson(model)));
  }

}
