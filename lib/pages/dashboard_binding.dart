import 'package:get/get.dart';
import 'package:nextbase_assignent/pages/chart_controller.dart';
import 'dashboard_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(() => DashboardController());
    Get.lazyPut<ChartController>(() => ChartController());
  }
}
