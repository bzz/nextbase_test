import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:nextbase_assignent/pages/maps_controller.dart';
import 'dashboard_controller.dart';

class MapsPage extends GetView<MapsController> {
  @override
  Widget build(BuildContext context) {
    final DashboardController c = Get.put(DashboardController());
    return Obx(() => Scaffold(
          appBar: AppBar(title: const Text('Map'), actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: ElevatedButton(
                  onPressed: () {
                    c.getData(0);
                  },
                  child: const Icon(
                    Icons.refresh,
                    size: 26.0,
                  ),
                )),
          ]),
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: FlutterMap(
              options: MapOptions(
                center: c.getCenterPoint(),
                zoom: 14.0,
              ),
              layers: [
                TileLayerOptions(
                    urlTemplate:
                        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                    subdomains: ['a', 'b', 'c']),
                PolylineLayerOptions(
                  polylines: [
                    Polyline(
                        points: c.getPoints(),
                        strokeWidth: 4.0,
                        color: Colors.red),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
