import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:nextbase_assignent/model/piece.dart';
import 'package:charts_common/src/common/color.dart' as chartColor;
import 'dashboard_controller.dart';
import 'chart_controller.dart';

class ChartPage extends GetView<ChartController> {
  @override
  Widget build(BuildContext context) {
    final DashboardController c = Get.put(DashboardController());
    _chart(double Function(Piece, dynamic) pp, chartColor.Color color) {
      return charts.LineChart([
        charts.Series<Piece, double>(
          id: 'Pieces',
          colorFn: (_, __) => color,
          areaColorFn: (_, __) => charts.MaterialPalette.transparent,
          measureFn: pp,
          domainFn: (Piece p, a) => (a == null ? 0 : a.toDouble()),
          measureLowerBoundFn: (Piece p, a) => -2,
          measureUpperBoundFn: (Piece p, a) => 2,
          data: c.data.toList(),
        )
      ], animate: false);
    }

    return Scaffold(
      appBar: AppBar(title: const Text('Chart'), actions: <Widget>[
        Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: ElevatedButton(
              onPressed: () {
                c.getData(0);
              },
              child: const Icon(
                Icons.refresh,
                size: 26.0,
              ),
            )),
      ]),
      body: Obx(
        () => c.data.isEmpty
            ? GestureDetector(
                onTap: () {
                  controller.loading.value = true;
                  c.getData(0);
                },
                child: Container(
                    color: Colors.transparent,
                    child: Center(
                        child: controller.loading.value
                            ? const CircularProgressIndicator()
                            : const Text('Tap to get data'))),
              )
            : Stack(children: [
                _chart((Piece p, a) => p.xAcc,
                    charts.MaterialPalette.red.shadeDefault),
                _chart((Piece p, a) => p.yAcc,
                    charts.MaterialPalette.green.shadeDefault),
                _chart((Piece p, a) => p.zAcc,
                    charts.MaterialPalette.blue.shadeDefault),
              ]),
      ),
    );
  }
}
