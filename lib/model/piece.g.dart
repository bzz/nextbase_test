// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'piece.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Piece _$$_PieceFromJson(Map<String, dynamic> json) => _$_Piece(
      bearing: (json['bearing'] as num?)?.toDouble(),
      datetime: json['datetime'] as String?,
      distanceFromLast: (json['distanceFromLast'] as num?)?.toDouble(),
      gpsStatus: json['gpsStatus'] as String?,
      lat: (json['lat'] as num).toDouble(),
      lon: (json['lon'] as num).toDouble(),
      speed: (json['speed'] as num?)?.toDouble(),
      xAcc: (json['xAcc'] as num).toDouble(),
      yAcc: (json['yAcc'] as num).toDouble(),
      zAcc: (json['zAcc'] as num).toDouble(),
    );

Map<String, dynamic> _$$_PieceToJson(_$_Piece instance) => <String, dynamic>{
      'bearing': instance.bearing,
      'datetime': instance.datetime,
      'distanceFromLast': instance.distanceFromLast,
      'gpsStatus': instance.gpsStatus,
      'lat': instance.lat,
      'lon': instance.lon,
      'speed': instance.speed,
      'xAcc': instance.xAcc,
      'yAcc': instance.yAcc,
      'zAcc': instance.zAcc,
    };
