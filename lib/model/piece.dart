import 'package:freezed_annotation/freezed_annotation.dart';

part 'piece.freezed.dart';

part 'piece.g.dart';

@freezed
class Piece with _$Piece {
  factory Piece({
    double? bearing,
    String? datetime,
    double? distanceFromLast,
    String? gpsStatus,
    required double lat,
    required double lon,
    double? speed,
    required double xAcc,
    required double yAcc,
    required double zAcc,
  }) = _Piece;

  factory Piece.fromJson(Map<String, dynamic> json) => _$PieceFromJson(json);
}
