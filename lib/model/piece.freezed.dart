// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'piece.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Piece _$PieceFromJson(Map<String, dynamic> json) {
  return _Piece.fromJson(json);
}

/// @nodoc
class _$PieceTearOff {
  const _$PieceTearOff();

  _Piece call(
      {double? bearing,
      String? datetime,
      double? distanceFromLast,
      String? gpsStatus,
      required double lat,
      required double lon,
      double? speed,
      required double xAcc,
      required double yAcc,
      required double zAcc}) {
    return _Piece(
      bearing: bearing,
      datetime: datetime,
      distanceFromLast: distanceFromLast,
      gpsStatus: gpsStatus,
      lat: lat,
      lon: lon,
      speed: speed,
      xAcc: xAcc,
      yAcc: yAcc,
      zAcc: zAcc,
    );
  }

  Piece fromJson(Map<String, Object?> json) {
    return Piece.fromJson(json);
  }
}

/// @nodoc
const $Piece = _$PieceTearOff();

/// @nodoc
mixin _$Piece {
  double? get bearing => throw _privateConstructorUsedError;
  String? get datetime => throw _privateConstructorUsedError;
  double? get distanceFromLast => throw _privateConstructorUsedError;
  String? get gpsStatus => throw _privateConstructorUsedError;
  double get lat => throw _privateConstructorUsedError;
  double get lon => throw _privateConstructorUsedError;
  double? get speed => throw _privateConstructorUsedError;
  double get xAcc => throw _privateConstructorUsedError;
  double get yAcc => throw _privateConstructorUsedError;
  double get zAcc => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PieceCopyWith<Piece> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PieceCopyWith<$Res> {
  factory $PieceCopyWith(Piece value, $Res Function(Piece) then) =
      _$PieceCopyWithImpl<$Res>;
  $Res call(
      {double? bearing,
      String? datetime,
      double? distanceFromLast,
      String? gpsStatus,
      double lat,
      double lon,
      double? speed,
      double xAcc,
      double yAcc,
      double zAcc});
}

/// @nodoc
class _$PieceCopyWithImpl<$Res> implements $PieceCopyWith<$Res> {
  _$PieceCopyWithImpl(this._value, this._then);

  final Piece _value;
  // ignore: unused_field
  final $Res Function(Piece) _then;

  @override
  $Res call({
    Object? bearing = freezed,
    Object? datetime = freezed,
    Object? distanceFromLast = freezed,
    Object? gpsStatus = freezed,
    Object? lat = freezed,
    Object? lon = freezed,
    Object? speed = freezed,
    Object? xAcc = freezed,
    Object? yAcc = freezed,
    Object? zAcc = freezed,
  }) {
    return _then(_value.copyWith(
      bearing: bearing == freezed
          ? _value.bearing
          : bearing // ignore: cast_nullable_to_non_nullable
              as double?,
      datetime: datetime == freezed
          ? _value.datetime
          : datetime // ignore: cast_nullable_to_non_nullable
              as String?,
      distanceFromLast: distanceFromLast == freezed
          ? _value.distanceFromLast
          : distanceFromLast // ignore: cast_nullable_to_non_nullable
              as double?,
      gpsStatus: gpsStatus == freezed
          ? _value.gpsStatus
          : gpsStatus // ignore: cast_nullable_to_non_nullable
              as String?,
      lat: lat == freezed
          ? _value.lat
          : lat // ignore: cast_nullable_to_non_nullable
              as double,
      lon: lon == freezed
          ? _value.lon
          : lon // ignore: cast_nullable_to_non_nullable
              as double,
      speed: speed == freezed
          ? _value.speed
          : speed // ignore: cast_nullable_to_non_nullable
              as double?,
      xAcc: xAcc == freezed
          ? _value.xAcc
          : xAcc // ignore: cast_nullable_to_non_nullable
              as double,
      yAcc: yAcc == freezed
          ? _value.yAcc
          : yAcc // ignore: cast_nullable_to_non_nullable
              as double,
      zAcc: zAcc == freezed
          ? _value.zAcc
          : zAcc // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
abstract class _$PieceCopyWith<$Res> implements $PieceCopyWith<$Res> {
  factory _$PieceCopyWith(_Piece value, $Res Function(_Piece) then) =
      __$PieceCopyWithImpl<$Res>;
  @override
  $Res call(
      {double? bearing,
      String? datetime,
      double? distanceFromLast,
      String? gpsStatus,
      double lat,
      double lon,
      double? speed,
      double xAcc,
      double yAcc,
      double zAcc});
}

/// @nodoc
class __$PieceCopyWithImpl<$Res> extends _$PieceCopyWithImpl<$Res>
    implements _$PieceCopyWith<$Res> {
  __$PieceCopyWithImpl(_Piece _value, $Res Function(_Piece) _then)
      : super(_value, (v) => _then(v as _Piece));

  @override
  _Piece get _value => super._value as _Piece;

  @override
  $Res call({
    Object? bearing = freezed,
    Object? datetime = freezed,
    Object? distanceFromLast = freezed,
    Object? gpsStatus = freezed,
    Object? lat = freezed,
    Object? lon = freezed,
    Object? speed = freezed,
    Object? xAcc = freezed,
    Object? yAcc = freezed,
    Object? zAcc = freezed,
  }) {
    return _then(_Piece(
      bearing: bearing == freezed
          ? _value.bearing
          : bearing // ignore: cast_nullable_to_non_nullable
              as double?,
      datetime: datetime == freezed
          ? _value.datetime
          : datetime // ignore: cast_nullable_to_non_nullable
              as String?,
      distanceFromLast: distanceFromLast == freezed
          ? _value.distanceFromLast
          : distanceFromLast // ignore: cast_nullable_to_non_nullable
              as double?,
      gpsStatus: gpsStatus == freezed
          ? _value.gpsStatus
          : gpsStatus // ignore: cast_nullable_to_non_nullable
              as String?,
      lat: lat == freezed
          ? _value.lat
          : lat // ignore: cast_nullable_to_non_nullable
              as double,
      lon: lon == freezed
          ? _value.lon
          : lon // ignore: cast_nullable_to_non_nullable
              as double,
      speed: speed == freezed
          ? _value.speed
          : speed // ignore: cast_nullable_to_non_nullable
              as double?,
      xAcc: xAcc == freezed
          ? _value.xAcc
          : xAcc // ignore: cast_nullable_to_non_nullable
              as double,
      yAcc: yAcc == freezed
          ? _value.yAcc
          : yAcc // ignore: cast_nullable_to_non_nullable
              as double,
      zAcc: zAcc == freezed
          ? _value.zAcc
          : zAcc // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Piece implements _Piece {
  _$_Piece(
      {this.bearing,
      this.datetime,
      this.distanceFromLast,
      this.gpsStatus,
      required this.lat,
      required this.lon,
      this.speed,
      required this.xAcc,
      required this.yAcc,
      required this.zAcc});

  factory _$_Piece.fromJson(Map<String, dynamic> json) =>
      _$$_PieceFromJson(json);

  @override
  final double? bearing;
  @override
  final String? datetime;
  @override
  final double? distanceFromLast;
  @override
  final String? gpsStatus;
  @override
  final double lat;
  @override
  final double lon;
  @override
  final double? speed;
  @override
  final double xAcc;
  @override
  final double yAcc;
  @override
  final double zAcc;

  @override
  String toString() {
    return 'Piece(bearing: $bearing, datetime: $datetime, distanceFromLast: $distanceFromLast, gpsStatus: $gpsStatus, lat: $lat, lon: $lon, speed: $speed, xAcc: $xAcc, yAcc: $yAcc, zAcc: $zAcc)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Piece &&
            (identical(other.bearing, bearing) || other.bearing == bearing) &&
            (identical(other.datetime, datetime) ||
                other.datetime == datetime) &&
            (identical(other.distanceFromLast, distanceFromLast) ||
                other.distanceFromLast == distanceFromLast) &&
            (identical(other.gpsStatus, gpsStatus) ||
                other.gpsStatus == gpsStatus) &&
            (identical(other.lat, lat) || other.lat == lat) &&
            (identical(other.lon, lon) || other.lon == lon) &&
            (identical(other.speed, speed) || other.speed == speed) &&
            (identical(other.xAcc, xAcc) || other.xAcc == xAcc) &&
            (identical(other.yAcc, yAcc) || other.yAcc == yAcc) &&
            (identical(other.zAcc, zAcc) || other.zAcc == zAcc));
  }

  @override
  int get hashCode => Object.hash(runtimeType, bearing, datetime,
      distanceFromLast, gpsStatus, lat, lon, speed, xAcc, yAcc, zAcc);

  @JsonKey(ignore: true)
  @override
  _$PieceCopyWith<_Piece> get copyWith =>
      __$PieceCopyWithImpl<_Piece>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PieceToJson(this);
  }
}

abstract class _Piece implements Piece {
  factory _Piece(
      {double? bearing,
      String? datetime,
      double? distanceFromLast,
      String? gpsStatus,
      required double lat,
      required double lon,
      double? speed,
      required double xAcc,
      required double yAcc,
      required double zAcc}) = _$_Piece;

  factory _Piece.fromJson(Map<String, dynamic> json) = _$_Piece.fromJson;

  @override
  double? get bearing;
  @override
  String? get datetime;
  @override
  double? get distanceFromLast;
  @override
  String? get gpsStatus;
  @override
  double get lat;
  @override
  double get lon;
  @override
  double? get speed;
  @override
  double get xAcc;
  @override
  double get yAcc;
  @override
  double get zAcc;
  @override
  @JsonKey(ignore: true)
  _$PieceCopyWith<_Piece> get copyWith => throw _privateConstructorUsedError;
}
