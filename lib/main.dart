import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Response;
import 'package:nextbase_assignent/pages/dashboard_page.dart';
import 'package:nextbase_assignent/pages/dashboard_controller.dart';
import 'package:nextbase_assignent/pages/dashboard_binding.dart';
import 'themes/app_theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static var pages = [
    GetPage(
      name: '/',
      page: () => DashboardPage(),
      binding: DashboardBinding(),
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: '/',
      getPages: pages,
      debugShowCheckedModeBanner: false,
      theme: AppTheme.light,
      darkTheme: AppTheme.dark,
      themeMode: ThemeMode.system,
    );
  }
}

