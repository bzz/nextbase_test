# nextbase_assignent

Nextbase test assignment

## Getting Started

API: https://us-central1-mynextbase-connect.cloudfunctions.net/sampleData?page=0. You pass a GET “page” parameter, by default 0, to retrieve the next set of data. All pages combined make for an entire journey. UI elements need to display the entire set of data. The response object contains:

data
An array of dictionaries with these values:
{
"bearing": 241.360000610352, "datetime": "2018-02-22T09:39:46Z", "distanceFromLast": 0, "gpsStatus": "A",
"lat": 51.571875,
"lon": -3.20286661783854,
"speed": 0.48332756917646,
"xAcc": 0.0390625,
"yAcc": -0.046875,
"zAcc": 0.0234375
}
hasMore True / False depending on whether there is more data available.
Tasks
1. Develop a basic data retrieval layer in the architecture you are most familiar with. Ensure the layer supports automatic retries:
   The API is very unstable. Once in a while, the API will return HTTP 500. At that moment, the download needs to retry.
2. Bring the data into a Flutter App and display the “xAcc”, “yAcc”, “zAcc” parameters in a combined line chart.
3. In a secondary page, show the “lat”/”lon” parameters within a simple Google/Apple/OSM Map within the App. The display should reflect the entire path of a journey.
4. Write unit, integration & widgets tests for downloading & displaying the chart.
5. Add a golden test to verify the chart is displayed correctly.
   What we are looking for
- Code structure & architecture decisions
- Insight in how you approached the problem of the unstable server
  How we test
  flutter test && flutter run, followed by visual check on the phone
  Recommended flutter packages
  bloc, freezed, charts_flutter, retry, dio